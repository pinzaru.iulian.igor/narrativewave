To run the project on your local machine you would need to have **docker** and **docker-componse** installed.
  
```
cd ./narrativewave  
docker-compose -f docker-compose-local.yml build  
docker-compose -f docker-compose-local.yml up -d  
```

  
To view the logs use the following command:  
`docker logs -f narrativewave_api`  
or   
`docker-compose -f docker-compose-local.yml logs -f`  
  
It takes a few minutes to fetch the dependencies and install all it needs.  
When it's ready to go, please enter http://localhost:8080 in your browser to get to the application. 
The :8080 port is used for the single page application frontend, :8000 is used for API. Feel free to change those in the docker-compose-local.yml if you need:   
```
    ports:  
    - "0.0.0.0:8080:8080"  
```

=>  
```
    ports:  
    - "0.0.0.0:12345:8080"  
```

  
The app is hosted at [https://narrativewave.naughtyprogrammer.com](https://narrativewave.naughtyprogrammer.com) as well. Feel free to visit it.  
    
You can also use djang_rest_framework api browse feature to see the data, it is available at this url:
[https://narrativewave.naughtyprogrammer.com/api/parquet-service/](https://narrativewave.naughtyprogrammer.com/api/parquet-service/)


The DRF api includes the following paths (examples):
```
- /api/parquet-service/assets/
- /api/parquet-service/assets/WTG01/columns/TimeStamp/
- /api/parquet-service/assets/WTG01/columns/TimeStamp/?start_date=2019-03-22&end_date=2021-03-12
```

