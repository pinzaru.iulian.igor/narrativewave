'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BASE_DOMAIN: '"http://localhost:8080"',
  BASE_API_URL: '"http://localhost:8000/api"'
})
