'use strict'
module.exports = {
  NODE_ENV: '"production"',
  BASE_DOMAIN: '"https://narrativewave.naughtyprogrammer.com"',
  BASE_API_URL: '"https://narrativewave.naughtyprogrammer.com/api"'
}
