import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import router from './router';


Vue.config.productionTip = false;

import VueLodash from 'vue-lodash';
import lodash from 'lodash';

// name is optional
Vue.use(VueLodash, { name: 'custom', lodash: lodash });

import "./styles/main.scss";

new Vue({
	vuetify,
	render: h => h(App),
	router
}).$mount('#app');
