import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);
export default new VueRouter({
	mode: 'history',
	routes: [
		{
			path: '',
			name: 'parquet-view',
			component: () => import(/* webpackChunkName:"parquet-view" */ `@/components/ParquetView.vue`)
		}
	]
});
