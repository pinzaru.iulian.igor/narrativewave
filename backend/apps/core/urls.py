from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, re_path

from apps.core.views import ParquetService, AssetsList

urlpatterns = [
    path('assets/', AssetsList.as_view(), name='assets'),
    re_path('^assets/(?P<asset>[A-Z0-9]+)/$', ParquetService.as_view(), name='parquet-service-asset'),
    re_path('^assets/(?P<asset>[A-Z0-9]+)/columns/(?P<column>[\W\w]+)/$', ParquetService.as_view(),
            name='parquet-service-asset-column'),
    # path('assets/<str:asset>/', ParquetService.as_view(), name='parquet-service-asset'),
    # path('assets/<str:asset>/columns/<str:column>/', ParquetService.as_view(), name='some-shit')
]
