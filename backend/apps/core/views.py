from django.shortcuts import render
from django.conf import settings
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import datetime
import pyspark

from pyspark.sql import SparkSession
from pyspark.sql.functions import split, col, lit, to_date, current_timestamp
from pyspark import SparkContext

from apps.core.models import Asset, Column
from apps.core.serializers import AssetSerializer, ColumnSerializer, ParquetQuerySerializer
import logging

logger = logging.getLogger('django.server')

# Globally assign '2gb' of memory per executor
# SparkContext.setSystemProperty('spark.executor.memory', '2g')
# sc = SparkContext("local", "narrativewave")


class AssetsList(APIView):
    def get_object(self, pk):
        try:
            return Asset.objects.get(pk=pk)
        except Asset.DoesNotExist:
            raise Http404

    def get(self, request):
        assets = Asset.objects.all()
        serializer = AssetSerializer(assets, many=True)
        return Response(serializer.data)


class ParquetService(APIView):

    def get(self, request, *args, **kwargs):
        asset = kwargs.get('asset', None)
        column = kwargs.get('column', None)
        query_params = dict()
        start_date = request.query_params.get('start_date')
        end_date = request.query_params.get('end_date')
        query_params['start_date'] = start_date
        query_params['end_date'] = end_date
        query_params['asset'] = asset
        query_params['column'] = column

        query_serializer = ParquetQuerySerializer(data=query_params)
        is_valid = query_serializer.is_valid(raise_exception=False)

        if is_valid:
            collected = self.collect_data(filters=query_params)
            return Response(collected)
        else:
            return Response(query_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def collect_data(self, filters):
        spark = SparkSession.builder.appName('narrativewave').getOrCreate()
        df = spark.read.parquet(str(settings.ROOT_DIR / "resulting_datasets"))
        df.printSchema()

        if filters.get('asset'):
            df = df.filter(df['asset'] == filters['asset'])

        if filters.get('column'):
            df = df.filter(df['column'] == filters['column'])

        if filters.get('start_date'):
            start_date = datetime.date.fromisoformat(filters.get('start_date'))
            df = df.filter(df.date_type > start_date)
            # df.where(df.date_type > start_date)  # alternative way

        if filters.get('end_date'):
            end_date = datetime.date.fromisoformat(filters.get('end_date'))
            df = df.filter(df.date_type < end_date)
            # df.where(df.date_type > end_date)  # alternative way

        result = [row.asDict() for row in df.collect()]

        return result
