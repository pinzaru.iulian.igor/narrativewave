from django.db import models
from apps.core.base_models import BaseModel


class Asset(BaseModel):
	name = models.CharField(max_length=256, blank=False, unique=True)
	columns = models.ManyToManyField(
		'Column',
		verbose_name='list of columns',
		through='AssetsColumns',
		through_fields=('asset', 'column')
	)

	def __str__(self):
		return "%s" % (self.name)


class Column(BaseModel):
	name = models.CharField(max_length=256, blank=False, unique=True)

	def __str__(self):
		return "%s" % (self.name)


class AssetsColumns(BaseModel):
	asset = models.ForeignKey(Asset, on_delete=models.CASCADE)
	column = models.ForeignKey(Column, on_delete=models.CASCADE)
