from rest_framework import serializers
from apps.core.models import Asset, Column
from datetime import datetime


class ColumnSerializer(serializers.ModelSerializer):
	id = serializers.IntegerField(read_only=True)
	name = serializers.CharField(required=False, allow_blank=False, max_length=512)

	class Meta:
		model = Column
		fields = ['id', 'name']


class AssetSerializer(serializers.ModelSerializer):
	id = serializers.IntegerField(read_only=True)
	name = serializers.CharField(required=False, allow_blank=False, max_length=512)
	columns = ColumnSerializer(many=True, read_only=True)

	class Meta:
		model = Asset
		fields = ['id', 'name', 'columns']


class ParquetQuerySerializer(serializers.Serializer):
	start_date = serializers.DateField(format='%Y-%m-%d', required=False, allow_null=True)
	end_date = serializers.DateField(format='%Y-%m-%d', required=False, allow_null=True)
	asset = serializers.CharField(max_length=512, required=False, allow_null=True)
	column = serializers.CharField(max_length=512, required=False, allow_null=True)

	def validate_asset(self, value):
		if value is None:
			return value
		qs = Asset.objects.filter(name=value)
		if qs.exists():
			return value
		raise serializers.ValidationError("No valid asset provided.")

	def validate_column(self, value):
		if value is None:
			return value
		qs = Column.objects.filter(name=value)
		if qs.exists():
			return value
		raise serializers.ValidationError("No valid column provided.")

	def validate(self, data):
		start_date = data.get('start_date')
		end_date = data.get('end_date')
		asset = data.get('asset')
		column = data.get('column')
		if start_date and end_date and start_date > end_date:
			raise serializers.ValidationError("Value for end_date must be after start_date.")
		if asset and column:
			# assuming that our database is always a source of truth for assets/column provided by parquet files
			qs = Asset.objects.filter(name=asset, columns__name=column)
			if not qs.exists():
				raise serializers.ValidationError("There is no column for such asset.")
		if start_date is None and end_date is None and asset is None and column is None:
			raise serializers.ValidationError("No values were provided to filter dataset.")
		return data

	def update(self, instance, validated_data):
		pass

	def create(self, validated_data):
		pass
