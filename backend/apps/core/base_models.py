from django.db import models


class BaseModel(models.Model):
    created_at = models.DateTimeField("Created at", auto_now_add=True, db_index=True)
    updated_at = models.DateTimeField("Updated at", auto_now=True, db_index=True)

    is_visible = models.BooleanField(default=True, db_index=True)

    class Meta:
        abstract = True
