import logging

from django.core.management.base import BaseCommand, CommandError
from apps.core.tasks import task_read_from_parquet 

logger = logging.getLogger(__name__)
logger.setLevel(3)


class Command(BaseCommand):

	help = """
		The purpose of this command is to store `assets` and matching `columns` from parquet files into database.
	""" 

	def handle(self, *args, **options):
		try:
			task_read_from_parquet()
		except Exception as ex:
			logger.error(ex)
			logger.error('Failed to read from parquet files. Please check if it was generated first.')
