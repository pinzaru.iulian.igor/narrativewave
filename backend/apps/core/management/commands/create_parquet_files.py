import logging

from django.core.management.base import BaseCommand, CommandError
from apps.core.tasks import task_convert_long_to_wide 

logger = logging.getLogger(__name__)
logger.setLevel(3)


class Command(BaseCommand):

	help = """
		The purpose of this command is to create a function that reads multiple csv files and stores the data in parquet partition format. 
		The partition should be by Asset, Year and Month.
	"""

	def handle(self, *args, **options):
		try:
			task_convert_long_to_wide()
		except Exception as ex:
			logger.error(ex)
			logger.error('Failed to convert long to wide.')
