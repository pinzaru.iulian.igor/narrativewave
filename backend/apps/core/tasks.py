import logging
import pyspark
from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.functions import split, col

from pathlib import Path
from django.conf import settings

from apps.core.models import Asset, Column

logger = logging.getLogger(__name__)


# Globally assign '2gb' of memory per executor
SparkContext.setSystemProperty('spark.executor.memory', '2g')
sc = SparkContext("local", "narrativewave2")


def task_convert_long_to_wide():
    """
        The purpose of this command is to read multiple csv files and stores the data in parquet partition format.
        The partition should be by Asset, Year and Month.
    """
    logger.info("Starting parquet files processing...")

    spark = SparkSession.builder.appName('narrativewave').getOrCreate()

    df2 = spark.read.option("header", True).csv(str(settings.ROOT_DIR / "challenge_datasets"))

    split_col = pyspark.sql.functions.split(df2['tag'], '/')
    df2 = df2.withColumn('site', split_col.getItem(1))
    df2 = df2.withColumn('asset', split_col.getItem(2))
    df2 = df2.withColumn('column', split_col.getItem(3))
    df2 = df2.withColumn('date_type', col('timestamp').cast('date'))
    df2 = df2.drop('tag')

    split_col = pyspark.sql.functions.split(df2['timestamp'], '-')
    df2 = df2.withColumn('year', split_col.getItem(0))
    df2 = df2.withColumn('month', split_col.getItem(1))

    df2.printSchema()

    Path('./resulting_datasets').mkdir(parents=True, exist_ok=True)
    df2.write.option("header", True).partitionBy("asset", "year", "month").mode("overwrite").parquet(
        "./resulting_datasets")


def task_read_from_parquet():
    """
        The purpose of this command is to store `assets` and matching `columns` from parquet files into database.
    """
    logger.info("Starting to collect parquet data into models...")

    spark = SparkSession.builder.appName('narrativewave').getOrCreate()

    df = spark.read.parquet(str(settings.ROOT_DIR / "resulting_datasets"))

    df_distinct_assets = df.select('asset').distinct().collect()

    for row in df_distinct_assets:
        df_distinct_columns = df[df['asset'] == row.asset].select('column').distinct().collect()
        related_columns = [row.column for row in df_distinct_columns]

        # save into model
        new_asset, created = Asset.objects.get_or_create(name=row.asset)

        for col_name in related_columns:
            new_column, col_created = Column.objects.get_or_create(name=col_name)
            new_asset.columns.add(new_column)
