from .common import *

# Local Dev overrides
DEBUG = True

ALLOWED_HOSTS = ['*', '127.0.0.1', '0.0.0.0']

CORS_ALLOWED_ORIGINS = [
    # "https://example.com",
    # "https://sub.example.com",
    "http://localhost:8080",
    "http://127.0.0.1:8080",
    "http://localhost:8081"
]
